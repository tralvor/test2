<?php

/**
 * Front Controller
 */
error_reporting(E_ALL);

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'App' . DIRECTORY_SEPARATOR);

/**
 * Autoload
 */
spl_autoload_register( function ($className) {
    
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';

    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    if (file_exists($fileName)) {
   		require $fileName;
    }
    else {
    	throw new Exception('file "'.$fileName.'" not found');
    }

});


/**
 * Router
 */
$pathArgs = explode('/', $_SERVER['REQUEST_URI']);
$className = 'App\\Controllers\\' . (!empty($pathArgs[1]) ?  ucfirst($pathArgs[1]) : 'Main');
$methodName = isset($pathArgs[2]) ? $pathArgs[2] : 'index';

try {
    if (method_exists($className, $methodName)) {
        $controller =new $className();
        $methodArgs = array_splice($pathArgs, 3);
        call_user_func_array(array($controller, $methodName), $methodArgs);
    }
    else {
        throw new Exception('class "'.$className.'" does not have a method "'.$methodName.'"');
    }
} catch (Exception $e) {
    echo '404 not found (' . $e->getMessage() . ')';
    die();
}
