<?php 

namespace App\Controllers;

use Core\Controller;
use App\Models\UserModel;
use App\Models\UserLogModel;
use Core\Validation\Validator;

class User extends Controller {

    function __construct() {}

    public function index() {

        $user = new UserModel;
        $data['users'] = $user->getAll();
        $this->view('users', $data);

    }

    public function add() {

        $user = new UserModel;
        
        if($_POST) {

            $validator = new Validator();
            $validator->addRule('name', 'Имя', 'notEmpty|unique/users:name|AlphaDash');
            
            if($validator->isValid($_POST)) {
                $user->create($validator->getData());
            } else {
                $data['errors'] = $validator->getErrors();
            }

        }
        
        $data['users'] = $user->getAll();
        $this->view('users', $data);

    }

    public function destroy($id) {

        $user = new UserModel;
        $user->destroy($id);

        $data['users'] = $user->getAll();
    	$this->view('users', $data);

    }

    public function clans_users() {

        $user = new UserModel;
        $result = $user->getUsersClans();
        $data['clans'] = array();
        foreach ($result as $k => $v) {
            $data['clans'][$v['clan_id']][] = $v;
        }
        
        $this->view('clans_users', $data);

    }

    public function log($user_id) {

        $userLog = new UserLogModel;
        $data['log'] = $userLog->getLog($user_id);
        $data['user_id'] = $user_id;
        $data['actions'] = array(1 => 'Вступил в клан', 2 => 'Покинул клан', 3 => 'Добавил очки');

        $this->view('user_log', $data);

    }

}