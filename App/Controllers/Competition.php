<?php 

namespace App\Controllers;

use Core\Controller;
use Core\Router;
use App\Models\CompetitionModel;
use App\Models\ResultsModel;
use App\Models\UserModel;
use App\Models\UserLogModel;
use App\Models\CompetitionLogModel;
use App\Models\ClanModel;
use Core\Validation\Validator;

class Competition extends Controller {

    function __construct() {}

    public function index() {

        $competition = new CompetitionModel;

        if($_POST) {

            $validator = new Validator();
            $validator->addRule('name', 'Название', 'notEmpty|unique/competitions:name|AlphaDash');
            
            if($validator->isValid($_POST)) {
                $competition->create($validator->getData());
            } else {
                $data['errors'] = $validator->getErrors();
            }

        }
        
        $data['competitions'] = $competition->getAll();
        $this->view('competitions', $data);

    }


    public function destroy($id) {

        $competition = new CompetitionModel;
        if($competition->destroy($id))
            Router::redirect('competition');

    }


    public function add_points() {

        $user = new UserModel;
        $clan = new ClanModel;
        $competition = new CompetitionModel;

        $data['users'] = $user->getAll();
        $data['clans'] = $clan->getAll();
        $data['competitions'] = $competition->getAll();

        if($_POST) {

            $validator = new Validator();
            $validator
                ->addRule('user_id', 'ID пользователя', 'notEmpty|exist/users')
                ->addRule('clan_id', 'ID клана', 'notEmpty|exist/clans')
                ->addRule('competition_id', 'ID соревнования', 'notEmpty|exist/competitions')
                ->addRule('points', 'Очки', 'notEmpty|integer')
            ;

            if($validator->isValid($_POST)) {

                $dbData = $validator->getData();

                if($clan->userExist($dbData['user_id'], $dbData['clan_id'])) {
                    
                    $userLog = new UserLogModel;
                    $logData = $dbData;
                    $logData['action_type'] = 3;
                    $logData['date_added'] = date("Y-m-d H:i:s");
                    $userLog->create($logData);

                    $competitionLog = new CompetitionLogModel;
                    $cLogData = $dbData;
                    $logData['date_added'] = date("Y-m-d H:i:s");
                    $competitionLog->create($logData);

                }
                else {
                    $data['errors'] = 'Пользователь с указанным ID не найден в клане';
                }

            } else {
                $data['errors'] = $validator->getErrors();
            }

        }

        $this->view('add_points', $data);

    }

}