<?php 

namespace App\Controllers;

use Core\Controller;
use App\Models\ClanModel;
use App\Models\UserModel;
use App\Models\UserLogModel;
use Core\Router;
use Core\Validation\Validator;

class Clan extends Controller {

    function __construct() {}

    public function index() {

        $clan = new ClanModel;
        $data['clans'] = $clan->getAll();
        $this->view('clans', $data);

    }

    public function add() {

        $clan = new ClanModel;
        
        if($_POST) {

            $validator = new Validator();
            $validator->addRule('title', 'Название', 'notEmpty|unique/clans:title|AlphaDash');

            if($validator->isValid($_POST)) {
                $clan->create($validator->getData());
            } else {
                $data['errors'] = $validator->getErrors();
            }

        }

        $data['clans'] = $clan->getAll();
        $this->view('clans', $data);

    }

    public function destroy($clan_id) {

        $clan = new ClanModel;
        if($clan->destroy($clan_id))
            Router::redirect('clan');

    }

    //clan_users
    public function edit($id) {
        
        $clan = new ClanModel;

        if($_POST) {

            $validator = new Validator();
            $validator->addRule('user_id', 'ID пользователя', 'notEmpty|unique/clan_users:user_id|exist/users');

            if($validator->isValid($_POST)) {
                $clan->insertClanUser($validator->getData());

                //user log
                $userLog = new UserLogModel;
                $logData = $validator->getData();
                $logData['action_type'] = 1;
                $logData['date_added'] = date("Y-m-d H:i:s");
                $userLog->create($logData);

            } else {
                $data['errors'] = $validator->getErrors();
            }

        }

        $user = new UserModel;

        $data['id'] = $id;
        $data['clan_users'] = $clan->getClanUsers($id);
        $data['users'] = $user->getAll();
        
        $this->view('clan_users', $data);

    }

    //delete user from clan
    public function delete_user($user_id, $clan_id) {

        $clan = new ClanModel;

        if($clan->deleteClanUser($user_id)) {

            //user log
            $userLog = new UserLogModel;
            $logData['user_id'] = $user_id;
            $logData['clan_id'] = $clan_id;
            $logData['action_type'] = 2;
            $logData['date_added'] = date("Y-m-d H:i:s");
            $userLog->create($logData);

            Router::redirect('clan/edit/'.$clan_id);

        }

    }

}