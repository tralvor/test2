<?php 

namespace App\Controllers;

use Core\Controller;
use App\Models\ResultsModel;

class Main extends Controller {

    function __construct() {}

    public function index() {

    	$results = new ResultsModel;

        if(isset($_POST['update_counts']))
            $results->updateCount();

        $data['results'] = $results->getAll();
        $this->view('results', $data);

    }

}