<?php

namespace App\Models;

use Core\Database\Model;

class UserLogModel extends Model {
	
	protected $tableName = 'users_log';

	//action_type:1 - join to clan, 2 - escape from clan, 3 - add points
	protected $fields = ['user_id', 'clan_id', 'competition_id', 'action_type', 'points', 'date_added'];


	public function getLog($user_ud) {

		return $this->db->getAll("SELECT * FROM $this->tableName WHERE user_id=?i", $user_ud);

	}

}