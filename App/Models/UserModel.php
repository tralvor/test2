<?php

namespace App\Models;

use Core\Database\Model;

class UserModel extends Model {
	
	protected $tableName = 'users';
	protected $fields = ['name'];


	public function getUsersClans() {

		return $this->db->getAll('SELECT u.name AS user_name, u.id AS user_id, c.title AS clan_title, c.id AS clan_id
									FROM clan_users AS cu
									INNER JOIN clans AS c ON cu.clan_id=c.id
									INNER JOIN users AS u ON cu.user_id=u.id
									ORDER BY clan_id ASC
									');

	}

}