<?php

namespace App\Models;

use Core\Database\Model;

class CompetitionLogModel extends Model {
	
	protected $tableName = 'competitions_log';
	protected $fields = ['user_id', 'clan_id', 'competition_id', 'points', 'date_added'];

}