<?php

namespace App\Models;

use Core\Database\Model;
use App\Models\ClanModel;

class ResultsModel extends Model {
	
	protected $tableName = 'clan_competitions_results';
	protected $fields = ['clan_id', 'competition_id', 'count'];

	public function updateCount() {

		$query = '	INSERT INTO '.$this->tableName.' (clan_id, competition_id, count) 
					
					SELECT log.clan_id, log.competition_id, SUM(log.points) 
					FROM competitions_log AS log
					GROUP BY log.clan_id,  log.competition_id
					
					ON DUPLICATE KEY UPDATE count=(
						SELECT SUM(log.points) 
						FROM competitions_log AS log
						WHERE log.clan_id='.$this->tableName.'.clan_id AND log.competition_id='.$this->tableName.'.competition_id
						)';

		$this->db->query($query);

	}

}