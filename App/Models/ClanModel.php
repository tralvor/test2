<?php

namespace App\Models;

use Core\Database\Model;

class ClanModel extends Model {
	
	protected $tableName = 'clans';
	protected $fields = ['title'];

	public function getClanUsers($id) {

		return $this->db->getAll('SELECT u.*
									FROM clan_users AS cs 
									INNER JOIN users AS u on u.id=cs.user_id 
									WHERE cs.clan_id=?i', $id);

	}

	public function insertClanUser($data) {
		return $this->db->query('INSERT INTO clan_users SET clan_id=?i, user_id=?i ON DUPLICATE KEY UPDATE user_id=?i', 
									$data['clan_id'], $data['user_id'], $data['user_id']);
	}

	public function deleteClanUser($id) {
		return $this->db->query("DELETE FROM clan_users WHERE user_id=?i", $id);
	}

	public function userExist($user_id, $clan_id) {

		return $this->db->getOne("SELECT count(*) FROM clan_users WHERE user_id=?i AND clan_id=?i", $user_id, $clan_id);


	}

}