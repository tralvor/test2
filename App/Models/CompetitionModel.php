<?php

namespace App\Models;

use Core\Database\Model;

class CompetitionModel extends Model {
	
	protected $tableName = 'competitions';
	protected $fields = ['name'];

}