<h1>Пользователи по кланам</h1>

<div class="row">
	<div class="col-md-6">
		
		<? foreach ($clans as $clan_title => $clan_users): ?>

			<h3>Список пользователей в клане <?=$clan_title?></h3>
			<table class="table">
			    <caption>
			        
			    <thead>
			        <tr>
			            <th>#</th>
			            <th>Имя</th>
			            <th></th>
			        </tr>
			    </thead>
			    <tbody>
			        <? foreach ($clan_users as $k => $u): ?>
			        	<tr>
				            <th scope="row"><?=$u['user_id']?></th>
				            <td><?=$u['user_name']?></td>
				            <td>
					            <a href="/clan/delete_user/<?=$u['user_id']?>/<?=$u['clan_id']?>" class="btn btn-sm btn-default">Удалить из клана</a>
				            </td>
				        </tr>
			        <? endforeach; ?>
			    </tbody>
			</table>

		<? endforeach; ?>

	</div>
</div>
