<h1>Лог пользователя ID:<?=$user_id?></h1>

<div class="row">
	<div class="col-md-8">
		<h3>Действия</h3>
		<table class="table">      
		    <thead>
		        <tr>
		            <th>#</th>
		            <th>Действие</th>
		            <th>ID клана</th>
		            <th>ID соревнования</th>
		            <th>Добавленные очки</th>
		            <th>Время</th>
		        </tr>
		    </thead>
		    <tbody>
		        <? foreach ($log as $k => $v): ?>
		        	<tr>
			            <th scope="row"><?=$v['id']?></th>
			            <td><?=$actions[$v['action_type']]?></td>
			            <td><?=$v['clan_id']?></td>
			            <td><?=$v['competition_id']?></td>
			            <td><?=$v['points']?></td>
			            <td><?=$v['date_added']?></td>
			        </tr>
		        <? endforeach; ?>
		    </tbody>
		</table>
	</div>
	
</div>


