<div class="col-md-6 col-md-offset-3">

<h3>Добавление очков в клан</h3>

<? if(!empty($errors)): ?>
	<div class="alert alert-danger" role="alert"><?=$errors?></div>
<? endif; ?>

<form action="" method="POST" autocomplete="off">
  
	<div class="form-group">
		<label for="exampleInputEmail1">Соревнование</label>
		<select class="form-control" name="competition_id">
			<option value="" selected disabled>ID соревнования</option>
			<? foreach ($competitions as $k => $v): ?>
		        <option value="<?=$v['id']?>"><?=$v['name']?>(ID:<?=$v['id']?>)</option>
		    <? endforeach; ?>
		</select>
	</div>

	<div class="form-group">
		<label for="exampleInputEmail1">Клан</label>
		<select class="form-control" name="clan_id">
			<option value="" selected disabled>ID клана</option>
			<? foreach ($clans as $k => $v): ?>
		        <option value="<?=$v['id']?>"><?=$v['title']?>(ID:<?=$v['id']?>)</option>
		    <? endforeach; ?>
		</select>
	</div>

	<div class="form-group">
		<label for="exampleInputEmail1">Пользователь</label>
		<select class="form-control" name="user_id">
			<option value="" selected disabled>ID пользователя</option>
			<? foreach ($users as $k => $v): ?>
		        <option value="<?=$v['id']?>"><?=$v['name']?>(ID:<?=$v['id']?>)</option>
		    <? endforeach; ?>
		</select>
	</div>

	<div class="form-group">
		<label for="exampleInputEmail1">Очки</label>
		<input type="number" class="form-control" id="title" placeholder="Очки" name="points">
	</div>
  
  <button type="submit" class="btn btn-default">Добавить</button>
</form>

</div>