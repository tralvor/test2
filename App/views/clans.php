<h1>Кланы</h1>

<div class="row">
	<div class="col-md-6">
		<h3>Список кланов</h3>
		<table class="table"> 
		    <thead>
		        <tr>
		            <th>#</th>
		            <th>Название</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <? foreach ($clans as $k => $u): ?>
		        	<tr>
			            <th scope="row"><?=$u['id']?></th>
			            <td><?=$u['title']?></td>
			            <td>
				            <a href="/clan/destroy/<?=$u['id']?>" class="btn btn-sm btn-default">Удалить</a>
				            <a href="/clan/edit/<?=$u['id']?>" class="btn btn-sm btn-default">Управление</a>	
			            </td>
			        </tr>
		        <? endforeach; ?>
		    </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<h3>Создать клан</h3>
		<? if(!empty($errors)): ?>
			<div class="alert alert-danger" role="alert"><?=$errors?></div>
		<? endif; ?>

		<form action="/clan/add" method="POST">
		  <div class="form-group">
		    <input type="text" class="form-control" id="title" placeholder="Название клана" name="title" >
		  </div>
		  <button type="submit" class="btn btn-default">Отправить</button>
		</form>
	</div>
</div>


