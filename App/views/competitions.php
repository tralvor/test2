<h1>Соревнования</h1>

<div class="row">
	<div class="col-md-6">
		<h3>Список соревнований</h3>
		<table class="table">    
		    <thead>
		        <tr>
		            <th>#</th>
		            <th>Название</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		         <? foreach ($competitions as $k => $v): ?>
		        	<tr>
			            <th scope="row"><?=$v['id']?></th>
			            <td><?=$v['name']?></td>
			            <td>
				            <a href="/competition/destroy/<?=$v['id']?>" class="btn btn-sm btn-default">Удалить</a>
			            </td>
			        </tr>
		        <? endforeach; ?>
		    </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<h3>Создать соревнование</h3>
		
		<? if(!empty($errors)): ?>
			<div class="alert alert-danger" role="alert"><?=$errors?></div>
		<? endif; ?>

		<form action="/competition" method="POST">
		  <div class="form-group">
		    <input type="text" class="form-control" id="title" placeholder="Название" name="name" required>
		  </div>
		  <button type="submit" class="btn btn-default">Отправить</button>
		</form>
	</div>
</div>

