<h1>Клан ID:<?=$id?></h1>

<div class="row">
	<div class="col-md-6">
		<h3>Список пользователей в клане</h3>
		<table class="table">
		    <caption>
		        
		    <thead>
		        <tr>
		            <th>#</th>
		            <th>Имя</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <? foreach ($clan_users as $k => $u): ?>
		        	<tr>
			            <th scope="row"><?=$u['id']?></th>
			            <td><?=$u['name']?></td>
			            <td>
				            <a href="/clan/delete_user/<?=$u['id']?>/<?=$id?>" class="btn btn-sm btn-default">Удалить из клана</a>
			            </td>
			        </tr>
		        <? endforeach; ?>
		    </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<h3>Добавить пользователя в клан</h3>
		
		<? if(!empty($errors)): ?>
			<div class="alert alert-danger" role="alert"><?=$errors?></div>
		<? endif; ?>

		<form action="/clan/edit/<?=$id?>" method="POST">
			<input type="hidden" name="clan_id" value="<?=$id?>">
			<div class="form-group">
			    <select name="user_id" class="form-control" required>
			    	<? foreach ($users as $k => $u): ?>
				    	<option value="<?=$u['id']?>"><?=$u['name']?></option>
				    <? endforeach; ?>
			    </select>
			</div>
			<button type="submit" class="btn btn-default">Добавить</button>
		</form>
	</div>
</div>
