<h1>Пользователи</h1>

<div class="row">
	<div class="col-md-6">
		<h3>Список пользователей</h3>
		<table class="table">
		    <thead>
		        <tr>
		            <th>#</th>
		            <th>Имя</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <? foreach ($users as $k => $u): ?>
		        	<tr>
			            <th scope="row"><?=$u['id']?></th>
			            <td><?=$u['name']?></td>
			            <td>
				            <a href="/user/destroy/<?=$u['id']?>" class="btn btn-sm btn-default">Удалить</a>
				            <a href="/user/log/<?=$u['id']?>" class="btn btn-sm btn-default">Лог действий</a>	
			            </td>
			        </tr>
		        <? endforeach; ?>
		    </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<h3>Создать пользователя</h3>
		
		<? if(!empty($errors)): ?>
			<div class="alert alert-danger" role="alert"><?=$errors?></div>
		<? endif; ?>

		<form action="/user/add" method="POST">
		  <div class="form-group">
		    <input type="text" class="form-control" id="title" placeholder="Имя" name="name">
		  </div>
		  <button type="submit" class="btn btn-default">Отправить</button>
		</form>
	</div>
</div>

