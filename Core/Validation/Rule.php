<?php

namespace Core\Validation;

interface Rule {

	public function validate($field, $value, $validator);

	public function getErrorMessage($field, $value, $validator);

}