<?php

namespace Core\Validation\Rule;

class AlphaDash implements \Core\Validation\Rule {

	protected $regex = '/^[а-яА-ЯA-Za-z0-9-_]*$/u';

	public function validate($field, $value, $validator) {

		if(empty($value)) 
			return true;

		return (bool) preg_match($this->regex, $value);

	} 

	public function getErrorMessage($field, $value, $validator) {

		return 'Поле "'.$validator->getLabel($field) . '" может содержать только буквы, цифры и - _';

	} 

} 