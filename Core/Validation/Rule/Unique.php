<?php

namespace Core\Validation\Rule;

use Core\Database\DB;

class Unique implements \Core\Validation\Rule {

	protected $tableName;
	protected $tableField;


	public function __construct($value) {

		$params = explode(':', $value);

		if(count($params) != 2) {
			throw new \InvalidArgumentException("Incorrect format. Use table_name:field_name");
		}

		list($this->tableName, $this->tableField) = $params;

	}

	public function validate($field, $value, $validator) {

		$db = DB::getInstance();
		$result = $db->getOne("SELECT count(*) FROM $this->tableName WHERE $this->tableField = ?s", $value);

		return empty($result);

	} 

	public function getErrorMessage($field, $value, $validator) {

		return 'Поле "'.$validator->getLabel($field) . '" должно быть уникальным для таблицы "'.$this->tableName.'"';

	} 

} 