<?php

namespace Core\Validation\Rule;

use Core\Database\DB;

class Exist implements \Core\Validation\Rule {

	protected $tableName;


	public function __construct($tableName) {

		$this->tableName = $tableName;

	}

	public function validate($field, $value, $validator) {

		$db = DB::getInstance();
		$result = $db->getOne("SELECT count(*) FROM $this->tableName WHERE id = ?i", $value);

		return !empty($result);

	} 

	public function getErrorMessage($field, $value, $validator) {

		return 'Запись с данным ID "'.$validator->getLabel($field) . '" отсутствует в таблице "'.$this->tableName.'"';

	} 

} 