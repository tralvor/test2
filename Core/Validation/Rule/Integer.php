<?php

namespace Core\Validation\Rule;

class Integer implements \Core\Validation\Rule {


	public function validate($field, $value, $validator) {

		return is_numeric($value);

	} 

	public function getErrorMessage($field, $value, $validator) {

		return 'Поле "'.$validator->getLabel($field) . '" может содержать только цифры';

	} 

} 