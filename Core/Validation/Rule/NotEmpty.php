<?php

namespace Core\Validation\Rule;

class Notempty implements \Core\Validation\Rule {


	public function validate($field, $value, $validator) {

		return !empty($value);

	} 

	public function getErrorMessage($field, $value, $validator) {

		return 'Поле "'.$validator->getLabel($field) . '" не должно быть пустым';

	} 

} 