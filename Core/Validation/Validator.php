<?php

namespace Core\Validation;

use Core\Validation\Rule;

/**
 * Validation input data
 * @author tralvor@yandex.ru
 */

Class Validator {

	protected $data;
	protected $fields;
	protected $labels;
	protected $rules;
	protected $errors;

	function __construct() {}

	public function addRule($field, $label, $rules){
		
		$rules = explode('|', $rules);
		$this->rules[$field] = $rules;

		$this->labels[$field] = $label;
		
		return $this;

	}

	protected function testRules() {
		
		if(empty($this->rules)) return array();

		$errors = array();

		foreach($this->rules as $field => $rules) {

			list($result, $error) = $this->testFieldRules($field, $rules);
			
			if(!$result) 
				$errors[$field] = $error;

		}

		return $errors;

	} 

	protected function testFieldRules($field, array $rules) {

		$value = isset($this->data[$field]) ? $this->data[$field] : null;

		foreach($rules as $rule) {

			list($result, $error) = $this->testRule($field, $value, $rule);
			
			if($result === false) 
				return array(false, $error);

		}

		return array(true, null);

	} 

	protected function testRule($field, $value, $rule) {
		
		$rule_params = explode('/', $rule);

		$ruleClass = __NAMESPACE__.'\\Rule\\'.ucfirst($rule_params[0]);
		$rule = new $ruleClass($rule_params[1]);

		$result = $rule->validate($field, $value, $this);
		
		if($result) {
			return array(true, null);
		} else {
			return array(false, $rule->getErrorMessage($field, $value, $this));
		}


	}

	public function isValid(array $data) {
		$this->data = $data;
		$this->errors = $this->testRules();
		return empty($this->errors);
	} 


	public function getErrors() {
		return implode('<br />',array_values($this->errors));
	} 

	public function getData() {
		return $this->data;
	} 

	public function getLabel($field) {
		return $this->labels[$field];
	} 

}