<?php 

namespace Core;

use Core\Config;

class Router {

	function __construct() {}

	public static function redirect($path = '') {

		$site_url = Config::get('app.site_url');

		if(empty($site_url)) {
			throw new \InvalidArgumentException("site_url is empty");
		}
		else {
			header('Location: '.$site_url.'/'.$path);
			die();
		}

		

	}


}