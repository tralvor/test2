<?

namespace Core\Database;

use Core\Database\SafeMySQL;
use Core\Config;

final class DB {

	private static $instance;

    private function __construct() {}

    private function __clone() {}

	public static function getInstance() {

        if (self::$instance === NULL) {

            $opts = Config::get('database');

            self::$instance = new SafeMySQL($opts);

        }

        return self::$instance;

    }


}