<?php

namespace Core\Database;

use Core\Database\DB;

/**
 * Simple model class
 * @author  tralvor@yandex.ru
 */
 
class Model {

	protected $db;
	protected $tableName;
	protected $id;
	protected $fields;

	function __construct() {
        
        $this->db = DB::getInstance();
	}

	public function destroy($id = false) {

		return $this->db->query("DELETE FROM $this->tableName WHERE id=?i", $id);       
         
	}


	public function create($request) {

		foreach ($this->fields as $k => $v) {
			if(isset($request[$v]))
				$data[$v] = "'" . $request[$v] . "'";
		}

		$fields = implode(',', array_keys($data));
		$values = implode(',', array_values($data));

		$sql = "INSERT INTO $this->tableName ( $fields ) VALUES ( $values )";
        
        return $this->db->query($sql);            
  
	}


	public function getAll() {

		return $this->db->getAll("SELECT * FROM $this->tableName");

	}

	public static function getOne($id) {

		return $this->db->getOne('SELECT * FROM $this->tableName WHERE id = ?i', $id);

	}


}