<?php

namespace Core;

/**
 * Simple config class
 * @author tralvor@yandex.ru
 */


final class Config {

    private static $instance;
    private static $items;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function __construct(){}
    
    public static function get($param) {

    	if(empty($param)) {
            return;
        }

        $instance = static::getInstance();
    	$params = explode('.', $param);
        $group = $params[0];
        unset($params[0]);
 	
    	$result = $instance->loadConfig($group);

        foreach ($params as $v) {
            if(isset($result[$v]))
                $result = $result[$v];
            else 
                break;
        }

        return $result;


    }

	private function loadConfig($group) {

		if(!isset(static::$items[$group])) {
            $fileName = APP.'Configs/' . $group . '.php';
            if(file_exists($fileName)) {
    			static::$items[$group] = include($fileName);
    		}
    		else {
    			static::$items[$group] = array();
    		}
        }

		return static::$items[$group];

	}

}