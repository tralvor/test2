<?php 

namespace Core;

class Controller {

    function __construct() {}

    protected function view($view, $data = array()) {
    	
    	extract($data);

    	ob_start();
    	require APP . 'views/head.php';
    	require APP . 'views/'.$view.'.php';
    	require APP . 'views/footer.php';
    	echo ob_get_clean();
        
    }

}